class Dictionary
  attr_reader :entries

  def initialize
    @entries = {}
  end

  def add(entry)
    if entry.is_a?(String)
      @entries[entry] = nil
    elsif entry.is_a?(Hash)
      @entries.merge!(entry)
    end
  end

  def find(frag)
    @entries.select do |w, definition|
      w.match(frag)
    end
  end

  def keywords
    @entries.keys.sort { |x, y| x <=> y }
  end

  def include?(w)
    @entries.has_key?(w)
  end

  def printable
    entries = keywords.map do |keyword|
      %Q{[#{keyword}] "#{@entries[keyword]}"}
    end

    entries.join("\n")
  end
end
